---
layout: page
pagekey: fakta
permalink: /fakta/
tags: page
title: Fakta
---

# {{title}}

Mennesker overbelaster Jordens klima og natur. Verdens befolkning vokser, og samtidig stiger det materielle forbrug og forureningen år for år. Der er nu [mere menneskeskabt materiale](https://www.nationalgeographic.com/environment/article/human-made-materials-now-equal-weight-of-all-life-on-earth) end levende organismer på Jorden. Det er uholdbart.

I år 1800 var Jordens befolkning på 1 milliard. Dengang boede der 1 million mennesker i Danmark. Siden er tallene vokset eksponentielt.
Nu, i 2023, er Jordens befolkning flere end 8 milliarder, og den vokser med 2 millioner mennesker om ugen. Om ugen!

Sådan ser befolkningstilvæksten ud fra for 10.000 år siden til år 2000:

<figure>

![Historic population curve](./Population_curve.svg)

<figcaption>

(Figur fra [Wikipedia](https://en.wikipedia.org/wiki/Population_growth#Human_population_growth_rate))

</figcaption>

</figure>

I Danmark er vi i øjeblikket cirka 5.800.000 indbyggere og forventes at nå 6 millioner om fem år. Disse ekstra 200.000 borgere skal have bolig, mad, osv. i samme omfang som os der allerede er her. De vil bidrage yderligere til Danmarks ekstremt høje ressourceforbrug og klimabelastning.

Her er en [befolknings-tæller](https://www.worldometers.info/world-population/) som opdateres i realtid.
Prøv at notere tallet, inden du går på facebook eller læser morgenavisen, og tjek det igen bagefter.
Der kan let være født fem tusind børn og kun sket nogle få tusind dødsfald – så verdens befolkning steg med tusindvis, mens du læste avisen.
En tilvækst af tusindvis af mennesker, som allesammen skal have bolig, mad, tøj, teknologi, transport og underholdning.

Bemærk at det er et nettotal: Mens cirka halvdelen af de nyfødte så at sige kan overtage den plads, som blev efterladt af en afdød, skal der til den anden halvdel udvindes flere mineraler, bygges nye boliger, anlægges veje og skoler og sygehuse, produceres ting og tøj og mad – af ressourcer, som før var fri natur. Og befolkningstilvæksten fortsætter, dag for dag, året rundt.

FN’s prognoser siger, at verdens befolkning vil blive 11 milliarder i år 2100, og derefter begynde et fald. Prognosen er lagt an på nogle antagelser om fødselstallene i verdens lande. Hvis fødselstallene kunne blive i gennemsnit ½ barn pr kvinde mindre, så vil befolkningstallet toppe allerede ved 10 milliarder i år 2080.

<figure>

![World population growth forecast](./world-total-population-with-estimates-UN.PNG)

<figcaption>

(Figur fra [FNs databank](https://population.un.org/wpp/Graphs/DemographicProfiles/Line/900))

</figcaption>

</figure>

Alle borgere i verden ønsker sig mad, tøj, en bolig, mobiltelefon og internet, transport, ferier, og andre ressourcekrævende forbrugsmuligheder. Vi, som allerede nyder godt af disse ting, kan ikke moralsk nægte vores medborgere det samme.

Men civilisationen forbruger ressourcer svarende til 1½ planeter. Det årlige ressourcebudget bliver opbrugt allerede i august – og datoen bliver tidligere år for år.
Væksten vil nødvendigvis stoppe på et tidspunkt. Det kan ske på voldsomme måder, med hungersnød, epidemier, flygtningestrømme, invasioner og krig – eller vi kan arbejde på at bremse væksten på fredeligere måder.

Befolkningstilvæksten standser ikke automatisk med uddannelse eller økonomisk udvikling. For eksempel havde Iran i 1970'erne en høj andel af piger i skole, og kvinder fik status af at uddanne sig; samtidig havde Iran et gennemsnitligt fødselstal på over 6 børn per kvinde.

Og omvendt er pigeuddannelse eller velstand ikke forudsætninger for at sænke fødselsraten. For eksempel sænkede Thailand sin fødselsrate i perioden 1962-1992 fra 7 til under 2 børn pr kvinde. En medvirkende drivkraft var ["Mister Condom"](https://en.wikipedia.org/wiki/Mechai_Viravaidya), en aktivist som promoverede sikker sex og prævention på en sjov og uhøjtidelig måde.

Mange afrikanske lande har fødselsrater over 5 børn pr kvinde, og en del lande i Asien og et enkelt i Sydamerika har fødselsrater over 3. Fødselsraterne i Nordamerika, Europa og Australien er mindre end 2.

Se [verdenskort over fødselstal](https://www.prb.org/international/indicator/fertility/snapshot/).
