---
layout: page
pagekey: svar
permalink: /svar/
tags: page
title: Svar
---

# Spørgsmål og svar

1. *Hader I børn?*
   - Nej, vi elsker børn. De er nuttede, sjove, kloge og dejlige, og børn udgør menneskehedens fremtid. Børn er hele motivet for, at vi skal passe bedre på kloden. I det lange løb skal menneskeheden sørge for at blive færre, netop for at dem, der findes, har bedre levevilkår.
   
1. *Er I nogle fascister, som vil bestemme over jeres medborgeres intime liv?*
   - Nej. Vi vil gerne gøre fødselstal og befolkningstal til stuerene emner, både i samfundsdebatten og i den enkelte borgers overvejelser om sin familiestørrelse. Ligesom for eksempel fakta om animalske fødevarers klima- og miljø-belastning skal indgå debatten om den grønne omstilling, bør også fakta om fødselstal og befolkningstal indgå. 

1. *Mener I, at vi skal ud og aflive vores medborgere?*
   - Nej, selvfølgelig ikke. Vi går ind for en fredelig, bevidst, langsigtet nedbringelse af befolkningstallet ved, at folk i en lang periode vælger at få færre børn.

1. *Er I nogle racister, som vil bestemme over u-landenes befolkningstal?*
   - Nej. Vi ønsker at bringe befolkningstal ind i samfundsdebatten, også som et relevant aspekt i u-lands-bistanden. Som donorland skal Danmark ikke forsøge at bestemme noget over det modtagende lands befolkningspolitik, men Danmarks bistandsprogrammer kan lægge endnu mere vægt på kvinders rettigheder, seksualoplysning i skoler, og adgang til prævention.

1. *Vil I bestemme, hvor mange børn jeg må få?* 
   - Nej, det skal du selv bestemme. Vi vil gerne gøre fødselstal og befolkningstal til emner, man kan tale sagligt og etisk om, både i samfundsdebatten og i den enkelte borgers overvejelser om sin familiestørrelse.

1. *Skal jeg have dårlig samvittighed over at have børn?*
   - Nej, dårlig samvittighed kan ikke bruges til noget. Vi må alle, som samfund og individer, tage udgangspunkt i den nuværende virkelighed. Derfra kan vi spørge os selv: Hvad kan jeg gøre? Noget af det bedste du kan gøre, er at lære alle de børn du kender – uanset om de er dine – at tage vare på Jordens klima og natur.

1. *Hvis fødselstallet falder under reproduktionsraten, uddør menneskene vel?*
   - Begrebet reproduktionsrate betyder, at hver generation får så mange børn, at næste generation vil blive lige så stor som deres forældres. Med det enorme antal mennesker vi er nu, overbelaster det Jorden at reproducere os i samme antal. Der er behov for en lang periode med faldende fødselsrate og dermed faldende befolkningstal, indtil vores antal er meget lavere, og det kan blive bæredygtigt at fødselsraten igen kommer op på reproduktionsraten.

1. *Verdens fødselsrate falder jo, så hvad er problemet?*
   - Fødselsraten er faldende, men den er stadig højere end reproduktionsraten. Problemet er den belastning, som det enorme antal mennesker udgør for Jordens ressourcer. Selv en optimistisk prognose forudsiger, at der skal skaffes livsgrundlag til mindst 2 milliarder flere, end vi er nu, før befolkningstallet topper. Ethvert tiltag, der kan nedbringe befolkningsvæksten, vil være til nytte for både naturen og menneskene.
   
1. *Danmarks befolkningstal er næsten stabilt, så vi har vel ikke noget problem?*
   - Jo, for Danmark overforbruger af verdens ressourcer. Hvis Danmark selv skulle dække vores ressourceforbrug, skulle landet være tre gange så stort. Hvis vi skulle bevare vores nuværende forbrug, burde vi være 2 millioner mennesker, ikke de nuværende 5,8 millioner. Danmarks bidrag til den globale grønne omstilling bør bestå i både sænket forbrug og sænket befolkningstal.

1. *Fødselstallet skal vel vokse for at sikre skatteborgere nok til vores pension?*
   - Hvis man mener at befolkningstallet skal blive ved med at vokse, så har man gang i et [pyramidespil](https://da.wikipedia.org/wiki/Pyramidespil), som kun kan ende galt. På et eller andet tidspunkt vil den kurve knække, og den ansvarlige beslutning vil være at se det i øjnene nu. Danmark skal finde en måde at forsørge alle borgere, også selvom der i en periode vil være flere gamle i forhold til unge, end vi tidligere har prøvet.

1. *Befolkningstilvækst er vel nødvendig for økonomisk vækst?*
   - Måske. Men det er nødvendigt standse den konstante tilvækst i materielt forbrug, som er tæt forbundet med økonomisk vækst. Vækstparadigmet driver rovdrift på Jordens ressourcer. Væksten kan ikke fortsætte i det uendelige på en begrænset klode.

1. *Findes der et perfekt befolkningstal?*
   - Hvor stort et bæredygtigt befolkningstal vil være, afhænger af den befolknings gennemsnitlige klima- og ressourceaftryk. Nogle forskere har opstillet [antagelser og beregninger](https://overpopulation-project.com/what-is-the-optimal-sustainable-population-size-of-humans/), der siger at 2-3 milliarder mennesker kunne have et liv med rimelig materiel velstand, og samtidig bevare en sund og robust klima og natur.
   
1. *Hvad kan jeg selv gøre?*
   - En vigtig beslutning er, hvor mange børn du vælger at få. Hvert barn er en ny forbruger, som forårsager [langt større CO2-udslip](https://www.kimnicholas.com/responding-to-climate-change.html) i sit liv, end en eksisterende forbruger kan spare ved hjælp af vegetarmad, grøn strøm og mindre flyvning. 
     Du kan hjælpe naturen ved at vælge at få færre børn, end du ellers ville have fået – om det så betyder færre end din mor fik, færre end vennerne har, færre end landsgennemsnittet eller reproduktionsraten.
     
1. *Hvis det betyder så meget for ressourcebelastningen at undlade at få 1 barn, kan barnløse så svine løs med god klimasamvittighed?*
    - Nej, vi skal arbejde på alle fronter. At sænke befolkningstallet er en langsigtet indsats. For at redde økosystemerne  og stabilisere klimaet, skal alle også handle på kort sigt. Klodens natur og klima har brug for al slags hjælp: fossil-stop, kulstofbindende landbrug, genetablering af naturområder – og færre mennesker.

1. *Hvordan kunne en dansk befolkningspolitik se ud?*
   - Se [vores forslag.](../politik)

1. *Med jeres foreslåede politik, så bliver det i langt højere grad de rige, som får råd til at stifte familier med 3 eller flere børn. Er det retfærdigt?*
    - I et markedsøkonomisk (ikke-kommunistisk) samfund som det danske vil der altid være en vis økonomisk ulighed mellem borgerne. En velfærdsstat som Danmark bruger sin skatte- og afgifts-politik til at mindske, men ikke helt at fjerne, disse forskelle. Det er rigtigt at med ’Befolkning i balance’s forslag om befolkningspolitik, kan rige mennesker have råd til at få flere end 2 børn, ligesom de kan have råd til at tage på flere ferier og købe en større bolig, end folk med lavere indtægter. Set i den store sammenhæng mener vi, at det er en acceptabel konsekvens af politikken.
    
1. *Prognoserne siger, at Jordens befolkningstal vil toppe ved 11 milliarder og derefter falde, så hvad er problemet?*
   - Der er et indlysende problem i udsigten til, at Jorden skal skaffe mad og ressourcer til yderligere 3 milliarder mennesker, foruden det antal vi allerede er. Dertil kommer, at mange af disse mennesker med rette vil forvente en bedre levestandard end deres forældre havde, og dermed presse landskab og naturressourcer endnu mere, end den nuværende befolkning allerede gør. 

1. *Så længe brune mennesker får mange børn, bør hvide mennesker vel også få mange børn, for ikke at blive udkonkurreret?*
   - Hvis man tænker sådan, så erklærer man våbenkapløb ved hjælp af børnefødsler. Det mener vi er uetisk. Vi mener, at hvert enkelt barn skal elskes som et menneske, ikke som en kamphandling. Vi mener at hudfarver blot er hudfarver, ikke markører om andre ting. Og vi mener at forskellige kulturer kontinuerligt bør være i dialog og lære af hinanden, så nye generationer vokser op endnu mere beriget. Hvis man synes, at éns egen kultur bør udbredes, så kan man gøre det på mange andre gode måder end at føde børn.

1. *Er klimaforandringer og naturødelæggelse ikke vigtigere faktorer end befolkningstal?*
   - Nej, for befolkningstallet bidrager direkte til belastningen af klima og natur. Faktorerne har effekt tilsammen.
   
1. *Befolkningstallet i fattige lande vil vel falde automatisk, når bare pigerne bliver uddannet?*
   - Nej. Der er en vis statistisk sammenhæng, men ingen automatik imellem pigers uddannelse og fødselstal. Regeringerne bør sætte målrettet ind på familieplanlæning, samtidig med at de hæver pigers uddannelsesniveau. (En dansk parallel kunne være, at der er en vis statistisk sammenhæng mellem forældres alkoholisme og misrøgt af børn – og kommunen nøjes jo ikke med at tilbyde alkohol-afvænning, men sætter også direkte ind overfor misrøgten.) 

1. *En del religioner opfordrer til, at man får mange børn – så jeres budskab om færre børn bliver vel overdøvet?*
   - Det er desværre sandt, at flere religioner er imod prævention. (Læs fx hos det [danske katolske kirkesamfund](https://www.katolsk.dk/tro/hvad-siger-kirken-om/sex-og-praevention/)). 
     Men de fleste religioner taler heldigvis også tydeligt for menneskers pligt til at passe på skaberværket. 
     Paven udsendte i 2015 en [erklæring om klima- og miljø-krisen](https://www.katolsk.dk/den-katolske-kirke-i-danmark/aktuelt/single-news/?tx_ttnews%5Btt_news%5D=14208&cHash=fda0acfaecf500738f128b63e03eb35b) 
     hvor han opfordrede alle til at se sig selv som en del af en samlet økologisk helhed. Erklæringen påpeger fint sammenhængen mellem et sundt samfund og en sund planet.
     Vi i ’Befolkning i balance’ er ikke selv religiøse. Men vi har tillid til, at de fleste religiøse samfund snart vil indse, at også *antallet* af mennesker på kloden har betydning for menneskenes samlede belastning af den. Indtil religionerne tør sige det eksplicit, vil vi holde dem op på den etiske pligt til at passe på skaberværket – og så må det foreløbig ligge mellem linjerne, at familieplanlægning og prævention bør være blandt løsningselementerne.
     
1. *Nytter det overhovedet noget? Er civilisationen ikke dødsdømt, uanset hvad vi gør?*
   - Vi tror, at det nytter at handle. Hver eneste gang du tager et klima- og natur-venligt valg, hjælper du til med, at udviklingen går i den rigtige retning. Og hver eneste gang du taler om bæredygtighed med dine venner og familie, hjælper du også dem med at blive bevidste om, hvad de kan gøre.
