---
layout: page
permalink: "/"
---

Velkommen til Befolkning i balance. Vi vil gerne af-tabuisere befolkningstal som emne i debatten om den grønne omstilling.

Mennesker overbelaster Jordens klima og natur. Verdens befolkning vokser, og samtidig stiger det materielle forbrug og forureningen år for år. Det er uholdbart.

For 10.000 år siden udgjorde hele menneskeheden 1% af alle hvirveldyr, og vilde dyr var 99%. Nu er menneskene 36% og vores husdyr 60%. De [vilde dyr er kun 4%.](https://www.greenpeace.org/international/story/17788/how-much-of-earths-biomass-is-affected-by-humans/) 

![illustration af en grøn og en overudnyttet klode](./begge-kloder.PNG)

Civilisationen er i fuld gang med at udrydde dyrearter og ødelægge økosystemer. Men vi kan ikke undvære naturen! Landskaber og vilde dyr er smukke og inspirerende. Biodiversiteten skaber robusthed overfor variationer i vind og vejr, udgør genpulje for fødevareforsyningen, og kan være grundlag for opdagelser af nye lægemidler og tekniske løsninger. Og naturen giver os gratis ’serviceydelser’ som rensning af luften, ferskvandet, havene og jorden, og bestøvning af afgrøder.

At sænke Jordens befolkningstal på langt sigt er nødvendigt for en bæredygtig verden. Ændringen bør selvfølgelig ske fredeligt og over flere generationer. Et mindre befolkningstal er afgørende for, at den enkelte verdensborger til den tid kan få adgang til rimeligt med ressourcer, og der samtidig er natur nok til at sikre biodiversitet, sundt klima og miljø, og fornyelse af ressourcerne.

Lige så vel som vi i klima- og miljø-debatten taler om, hvordan borgernes *gennemsnitlige belastning* af Jordens ressourcer kan nedbringes, bør vi tale om, hvordan *antallet af borgere* kan nedbringes på en bevidst og fredelig måde. Begge tal i gange-stykket bidrager til civilisationens belastning af Jorden, og begge sider af rektanglet skal blive kortere.


![illustration af befolkningstal gange forbrug](./gangestykket-flerfarvet.PNG)

Befolkningsspørgsmålet er desværre tabuiseret i samfundsdebatten. Emnet fremprovokerer stærke følelser, så det kan være svært at tale om. Der findes slemme historiske fortilfælde af voldelig befolkningskontrol, som vi absolut skal undgå at gentage. Men som ansvarlige samfundsborgere bør vi kunne tale nøgternt om vigtige emner, selvom de er vanskelige, og vi må kræve det samme af vores politikere. Befolkningstal og befolkningspolitik er sådan et vigtigt emne.

‘Befolkning i balance’ vil gerne bidrage til, at befolkningspolitik bliver et stuerent emne. Vær med!


