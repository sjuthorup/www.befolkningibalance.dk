---
layout: page
pagekey: politik
permalink: /politik/
tags: page
title: Politik
---

# {{title}}

I debatten om at passe på Jordens klima og natur bør vi kunne tale om antallet af mennesker. Det gælder både for kloden som helhed og i vores lille land. Som ansvarlige borgere må vi kunne tale sagligt om, hvordan en langsigtet påvirkning af befolkningstallene er et vigtigt element i den grønne omstilling. 

Stater benytter mange måder til at påvirke borgernes adfærd. Det mest kendte eksempel på en befolkningspolitik er [Kinas i perioden 1980-2015](https://en.wikipedia.org/wiki/One-child_policy). Da havde Kina en officiel ét-barns-politik, som blev håndhævet med bøder, men også med fængsling, påtvunget prævention og aborter, og tvangssterilisering. 

Sådanne voldsbaserede metoder til at kontrollere befolkningen tager vi i *Befolkning i balance* absolut afstand fra. Vi går 100% ind for demokrati og menneskerettigheder, og vores oplysningsarbejde er en del af den demokratiske samtale i den danske samfundsdebat.

Forbud og påbud er én del af lovgivnings-værktøjskassen, mens meget anden regulering sker via skatter, tilskud og afgifter. Ligesom cigaretter er højt beskattet, men ikke forbudt, kan man sagtens forestille sig nogle fredelige, langsigtede måder at sænke fødselstallet på. Ligesom vi må forvente højere priser på klima-belastende madvarer som kød, tropisk frugt og oversøisk vin, kan omkostningerne ved at få børn gøres synlige og mærkbare, og dermed påvirke borgernes adfærd.

En fornuftig dansk befolkningspolitik bør indeholde følgende elementer.

Direkte på fødsels- og familie-området:
* Gratis prævention.
* Fortsat fri og gratis adgang til abort.
* Oplysningskampagner om sterilisation, og operationen skal fortsat være gratis.
* Kun barselsorlov, børnecheck, pasningstilskud osv. til og med familiens andet barn. (For at fungere i forhold til skilsmisser, kan det i praksis udmøntes sådan, at hver enkelt voksen har ret til offentligt tilskud til ét barns forsørgelse.)
* Kun offentlig hjælp til fertilitetsbehandling for at få det første barn.
* Det offentlige tilskud til adoption hæves, så det dækker hele den faktiske omkostning.

Og Danmarks befolkningspolitik skal reflekteres i u-lands-bistanden:
*	I Danmarks bistand til udviklingslande skal lægges endnu mere vægt på at sikre pigers uddannelse, kvinders rettigheder, seksualoplysning, og borgeres adgang til nem og billig prævention og sterilisation.
*   Danmarks egen politik om gratis og lettilgængelig prævention, abort og sterilisation giver troværdighed til vores ønske som donor, at modtagerlandet gør tilsvarende for deres borgere.

I en periode vil Danmark og mange andre lande have ”for mange gamle” borgere i forhold til unge. I stedet for at se det som et problem, kan man anse det for en tilbagebetaling af den gevinst, som samfundet fik i den periode (1900-tallet), hvor befolkningen voksede, og landet derfor havde ”for mange unge” i forhold til gamle.

Rimeligvis bør pensionsalderen løbende justeres sådan, at forsørgerbyrden er overkommelig for dem, der er på arbejdsmarkedet. 
Dengang [folkepensionen](https://da.wikipedia.org/wiki/Folkepension) blev etableret, var levealderen ca 5 år højere end pensionsalderen, dvs gennemsnitligt ville en borger leve fem år som pensionist. Folk lever stadigt længere og med bedre helbred, så nye pensionister idag kan forvente at leve [gennemsnitligt 20 år](https://www.pensionforalle.dk/pension-helt-enkelt/hjaelp-til-selvhjaelp/hvor-gammel-bliver-du-se-din-forventede-levealder) som pensionist. I historisk lys vil det være naturligt at hæve pensionsalderen en hel del.  

Et tankeeksperiment:
Alle mennesker får ét barn. Fire mennesker vil dermed skabe to nye mennesker i den næste generation, og de to vil skabe ét nyt menneske i næste generation igen. 
Enhver kan blive forældre hvis de ønsker det, og samtidig falder befolkningstallet gradvis. Tankeeksperimentet ville i løbet af 100 år sænke verdens befolkning til et langt mere bæredygtigt antal på 3 milliarder.
Læs mere hos kampagnen ['One Planet, One Child'](https://oneplanetonechild.org/) som promoverer små familiestørrelser.

I takt med at befolkningstallet falder, kan naturen få god plads igen. Måske kan vi nå frem til visionen om [”Half Earth”](https://www.half-earthproject.org/discover-half-earth/): På halvdelen af Jordens areal kan menneskene bo, dyrke og holde husdyr, arbejde, lege osv. Og på den anden halvdel kan den vilde natur udfolde sig uforstyrret og skabe et robust globalt økosystem. 

![illustration af 'half earth'-tanken](./glimpse-152-half-earth-fullsize-grøn.png)