---
layout: page
pagekey: links
permalink: /links/
tags: page
title: Links
---

## Andre kilder

En lang række internationale organisationer arbejder indenfor området befolkningstal, fødselstal, seksualoplysning, familieplanlægning, og kvinders rettigheder.
De nedenstående websider er alle på engelsk. Bliv inspireret ved at læse om deres vigtige projekter.

[Population Matters](https://populationmatters.org/take-action). Denne britiske organisation har mange år på bagen og laver flotte oplysningskampagner, også internationalt. Blandt deres fortalere er den berømte naturkender [David Attenborough](https://populationmatters.org/news/2020/10/attenborough-film-saving-our-planet-requires-ending-population-growth) og racerkøreren [Leilani Münther](https://populationmatters.org/news/2018/05/14/our-new-patron-%22vegan-hippy-chick-race-car%22).

[World Population Balance](https://www.worldpopulationbalance.org/) er amerikansk og startet af holdet bag filmen [Growthbusters](https://www.growthbusters.org/economic-growth/). Organisationen laver oplysningskampagner, blandt andet "One Planet, One Child" og podcasten "The Overpopulation Podcast", se nedenfor. World Population Balance's website har også gode forklaringer, bl.a om [eksponentiel vækst](https://www.worldpopulationbalance.org/understanding-exponential-growth).

[”One Planet, One Child”](https://oneplanetonechild.org/) har det enkle og slående budskab, at hvis alle kvinder får højst ét barn, kan Jordens befolkning reduceres til bare 3 milliarder i løbet af 100 år. Budskabet har kørt som plakatkampagne i det nordvestlige USA og Canada.

[The Overpopulation Podcast](https://www.worldpopulationbalance.org/podcasts) præsenterer sig således: "Vi tør tale på vegne af de kommende generationer" – Om specifikke emner, hvor vækst og befolkningstal betyder noget. Hør bl.a nr 59 om befolkningstallets fald under corona-epidemien eller nr 55 om, at en voksende gennemsnitsalder ikke er noget samfundsproblem. Podcasten kan også følges på [facebook](https://www.facebook.com/watch/WorldPopulationBalance/).

[Church and State](http://churchandstate.org.uk/category/environment-and-population/) er en britisk religionskritisk organisation. Deres videoer og artikler indeholder mange gode overvejelser om menneskehedens indvirkning på, og afhængighed af naturen, inklusive skarp kritik af de store religioners store ansvar og manglende ansvarlighed overfor befolkningstilvæksten.

[Population Media Center](https://www.populationmedia.org/our-approach/) producerer populære underholdningsserier til radio og fjernsyn. Serierne indfletter seksualoplysning og budskaber om pigers og kvinders rettigheder. PMC er gode til at måle på effekten af deres serier, så de løbende får feedback om, hvordan deres budskaber bliver modtaget af befolkningen. Her er [en artikel](https://www.theguardian.com/global-development/2019/oct/17/soap-opera-could-be-unlikely-form-birth-control-uganda) om en af deres seriers succes i Uganda.

[MSI Reproductive Choices](https://www.msichoices.org/what-we-do/our-services/) er en hjælpeorganisation med 40 års erfaring, der udbreder familieplanlægning i fattige lande. De yder både oplysning, prævention, abort og efterfødsels-omsorg. Donationer til dem vil blive brugt direkte til at hjælpe kvinder i fattige lande med selv at planlægge, hvor mange børn de får og hvornår.

[Sustainable Population Australia](https://population.org.au/about-population/global-population/) har gode artikler, bl.a om sammenhængen mellem fødevareproduktion og befolkningstilvækst, og punktering af almindelige myter om befolkningsspørgsmål.

[Population Reference Bureau](https://www.prb.org/population-change/) indsamler data og offentliggør statistikker og analyser om sammenhængene mellem befolkningstal, sundhed og miljøbelastning.

[Seth Wynes's specialeprojekt](https://www.kimnicholas.com/responding-to-climate-change.html) undersøgte, hvilke personlige handlinger der kan have den største effekt for klimaet. Hans konklusion blev, at dét at få et barn mindre, end man ellers ville have fået, giver en langt større effekt end alle andre tiltag som at blive veganer, sortere sit skrald, droppe flyrejser eller køre i elbil. På websitet kan man downloade den videnskabelige artikel og grafik der viser tallene.

<figure>

![personal choices to abate climate change](./wynes-nicholas-poster-small.png)

<figcaption>

(Grafik fra ovennævnte website.)

</figcaption>

</figure>

[”Underestimating the Challenges of Avoiding a Ghastly Future”](https://www.frontiersin.org/articles/10.3389/fcosc.2020.615419/full#h9), Bradshaw et al, 2020 – Dette er egentlig en forskningsartikel, men den kan sagtens læses af en interesseret (og engelsk-kyndig) lægperson. Forfatterne er en lang række kendte navne fra de seneste årtiers forskning i de globale ressource-udfordringer. Hvis du har en god ven, som er modtagelig for fakta-baseret information men ikke helt har forstået alvoren i klima- og natur-krisen, kan artiklen være et godt udgangspunkt for en snak.
