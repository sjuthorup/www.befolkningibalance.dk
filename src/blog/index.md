---
layout: page
pagekey: blog
permalink: /blog/
tags: page
title: Blog
---

# Blog

_13 juni 2021 // Sju_

**Der fødes dobbelt så mange, som der dør**

Jeg blev klogere i denne uge. Politiken havde en forsideartikel med overskrift ”Skrækscenariet er vendt rundt. Nu kommer færre børn til verden”. Selvom jeg var glad for, at en stor dansk avis dækkede emnet befolkningstal, mente jeg ikke det kunne passe, at toppen var nået.

Derfor slog jeg op hos [Our World in Data](https://ourworldindata.org/grapher/births-and-deaths-projected-to-2100?country=~OWID_WRL). Det er en nonprofit-organisation, der indsamler data fra pålidelige kilder i hele verden og formidler det i datasæt, som man kan dowloade, og letforståelige grafer. Denne graf over fødsler og dødstal er baseret på FNs data og derfor må anses for en ret autoritativ kilde (gengivet nedenfor, ved siden af avisforsiden).

Grafen viser, at der i øjeblikket fødes mere end dobbelt så mange mennesker = den blå kurve, som antallet der dør = den røde kurve.

Sidste år var der ca 140 millioner fødsler og ca 60 millioner dødsfald. Så netto tilvæksten af mennesker på jorden, alene i 2020, var 80 millioner. Et enormt tal i sig selv, og endnu mere frygtindgydende når man tænker på, at i år vil tilvæksten også være ca 80 millioner, og igen i 2022, og 2023, og så videre.

I store træk har den blå fødselstals-kurve ligget på ca 120 millioner siden år 2010. Hvis man nærlæser den (på Our World in Datas side: hold musen over grafen), kan man se at der er en lille variation over de sidste ti år. Det højeste realiserede tal på fødselskurven er 140,25 millioner i år 2017, hvorefter det falder en smule til 139,98 millioner sidste år.

Grafen viser også **prognoser** for fødsler og dødsfald frem til sidst i vores århundrede: Den blå stiplede linje viser, at det årlige fødselstal forventes at falde til lidt over 139 millioner omkring år 2028, komme op over 140 mio igen i perioden 2037-2052, og først fra 2053 begynde at falde. I store træk forventes fødselstallet altså at ligge nogenlunde stabilt i fyrre år mere, og derefter langsomt falde støt.

Den røde stiplede linje viser, at antallet af årlige dødsfald forventes at stige støt og omkring år 2100 blive omtrent lig med fødselstallet, begge på lidt over 120 millioner årligt.

Alt dette betyder, at Politiken-overskriftens formulering "Der kommer færre børn til verden":
* kun gælder snævert for nogle få nylige årstal,
* kun gælder hvis man hæfter sig ved meget små tal,
* og formodentlig vil blive helt usandt fra år 2028.

Politiken kunne have formuleret en overskrift i stil med ”Vi har nået toppen”, hvis man synes at en ”top” godt kan være flad og dække halvtreds år. Men man kan kun i en meget snæver forstand påstå, at der kommer _færre_ børn til verden end der har gjort før – det vil først blive sandt om cirka fyrre år, ifølge FNs prognose.

Også den første halvdel af Politikens overskrift: ”Skrækscenariet er vendt rundt” er jeg skeptisk overfor. Der er endnu intet, der er vendt rundt. Vi kan stadig forvente at skulle give plads til 3 milliarder flere mennesker, før befolkningstallet vil begynde at falde ifølge prognoserne. I lyset af den igangværende klimakrise og naturudryddelser, mener jeg bestemt, at vi stadig har et skrækscenarie foran os.

Men jeg påskønner at have kigget nærmere på tallene og blevet klogere: At den stiplede blå kurve er stort set flad allerede og forventes at fortsætte sådan i de næste årtier, indtil den begynder at falde. Det var bedre end jeg frygtede. Min opsummering om verdens aktuelle befolkningssituation vil dog stadig være: Der fødes dobbelt så mange, som der dør.

_Efterskrift: Jeg sendte ovenstående blogindlæg til Politikens 'Fejl og fakta'-redaktør, som nu har ændret artiklens overskrift på deres web-avis til "Skrækscenariet ser ud til at vende. Fødselsraten falder verden over". Det er nærmere på sandheden._

<figure>

![(Udsnit af Politikens forside 8 juni 2021 – Graf fra Our World in Data, juni 2021)](./polforside-fødsler-døde-grafer.PNG)

<figcaption>

_(Udsnit af Politikens forside 8 juni 2021 – Graf fra Our World in Data, juni 2021)_

</figcaption>

</figure>


<br>

_2 juni 2021 // Sju_

**Kinas nye 3-barns-politik**

Kina er [verdens folkerigeste land]( https://en.wikipedia.org/wiki/List_of_countries_and_dependencies_by_population), med 1.410.000.000 indbyggere. Kinas regering har netop bekendtgjort, at det lovlige antal børn pr. familie sættes op fra 2 til 3. Hensigten er at øge antallet af børnefødsler og modvirke et ellers forventet snarligt fald i landets høje befolkningstal.

Denne politik er et markant eksempel på, hvordan politikere ikke indtænker klima- og natur-problemer i andre politikområder. I Danmark lover regeringen gladeligt 70% reduktion i CO2-udslippet, samtidig med at den skærmer landbruget og transportpolitikken fra forandring. På samme måde har Kina bekendtgjort en plan om [”net zero” CO2-udslip i 2060]( http://climate.org/chinas-net-zero-promise/) og nu, mindre end et år efter, en politik til at fastholde et fortsat højt befolkningstal.

Desværre ser mange journalister også snævert på befolkningspolitik. De artikler om nyheden, som jeg har læst i danske medier, nævner ikke hvad den nye politik vil betyde for klima og natur. Tag for eksempel [omtalen i Berlingske](https://www.berlingske.dk/globalt/det-var-et-af-verdenshistoriens-storste-sociale-eksperimenter-og-nu-lemper).
Artiklen gengiver klart Kinas begrundelse for sin tidligere étbarns-politik: ”at dæmpe presset på landets ressourcer” men straks efter begrundes den nye trebarns-politik med at ”holde hjulene i samfundet i gang”. De to ting bliver ikke relateret. En dansk Kina-forsker citeres: ”Problemet er, påpeger Ayo Wahlberg, at der ikke er de store incitamenter til at føde flere børn i nutidens Kina.” Læseren efterlades med en opfattelse af, at færre børnefødsler er problematisk, og at borgerne bør gives incitamenter til at få flere børn.

Men næsten alle lande i verden har et ressourceforbrug, som ikke er bæredygtigt, også Kina. Kinas mange borgere bliver stadigt rigere, mere forbrugende og mere forurenende. Befolkningstallet er større end nogensinde, og folk kører mere i bil, flyver mere på ferie, spiser mere kød og andre klimabelastende fødevarer, ønsker sig mere elektronik, forbruger flere varer og ophober mere skrald end nogensinde. Et faldende befolkningstal vil være et glimrende skridt i retning af en bæredygtig fremtid.

Jeg håber at Berlingske vil følge op på sin artikel ved at relatere Kinas tidligere begrundelse om ressourcebelastning med den nye befolkningspolitik. Den opfordring sender jeg nu til journalisten. Jeg vil opdatere her, hvis jeg får et svar.

<figure>

![Berlingskes artikel om Kinas nye trebarns-politik, 2 juni 2021 ](./2021-06-02-berlingske-trebarnspolitik.jpg)

<figcaption>

(Berlingskes artikel om Kinas nye trebarns-politik, 2 juni 2021)

</figcaption>

</figure>

<br>

_8 maj 2021 // Sju_

**Et væddemål om fødevaremangel**

Den britiske søndagsavis [The Observer skriver idag](https://www.theguardian.com/world/2021/may/08/are-there-too-many-people-all-bets-are-off) om et nyligt afgjort væddemål: En befolkningsforsker og en økonom væddede for ti år siden, om de globale fødevarepriser ville være steget eller faldet om 10 år, dvs nu.
Priserne kunne stige på grund af stor efterspørgsel – på grund af verdens stigende befolkningstal (og måske folks større købedygtighed, hvis velstanden generelt ville stige). Eller priserne kunne falde, fordi civilisationen igen og igen finder mere effektive måder at producere alting på, også fødevarer.

Befolkningsforskeren vandt – altså ham der følte, at befolkningspresset nødvendigvis måtte føre til prisstigninger.
De to parter i væddemålet afgjorde prisforskellen ud fra en defineret ”kurv” af dagligvarer, som i 2010 havde en værdi af ca 6000 kr.
Gevinsten i væddemålet var, at taberen – økonomen – måtte indbetale prisforskellen til en organisation, der arbejder for oplysning om befolkningsproblemstillinger.
Han sendte et beløb der svarer til 1200 kr, til [Population Media Center](https://www.populationmedia.org/our-approach/). Et repræsentativt udsnit af dagligvarer var altså steget med 20% på 10 år. I samme periode var Jordens befolkningstal steget med ca 850 millioner mennesker.

I artiklen interviewes en anden befolkningsforsker, John Bongaarts fra organisationen [Population Council](https://www.popcouncil.org/). Han kommenterer, at optimisterne i forhold til menneskers levevilkår ofte er økonomer, mens pessimisterne findes blandt forskere i naturvidenskaberne.

John Bongaarts siger tankevækkende: ”Optimister har tendens til at synes, at det er i orden at konvertere naturkapital til noget mennesker har nytte af. Mens pessimister ser nedgangen i naturkapital som et problem af socio-økonomiske, sundhedsmæssige og etiske grunde. Mange optimister påskønner ikke naturen lige så meget som mange pessimister gør.”

Hvad er du? Optimist eller pessimist? [Artiklen](https://www.theguardian.com/world/2021/may/08/are-there-too-many-people-all-bets-are-off) er værd at læse for en aktuel refleksion over klodens befolkningstal og befolkningstilvækst.

<figure>

![Richland Gardens building complex, Hong Kong](./Richland_Gardens_Hong_Kong.jpg)

<figcaption>

(Mange mennesker samlet på meget lille plads. Foto fra [Wikipedias opslag](https://commons.wikimedia.org/w/index.php?curid=4412593) om et højhuskompleks i Hong Kong, fotograf: Baycrest)

</figcaption>

</figure>
