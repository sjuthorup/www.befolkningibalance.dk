module.exports = {
  baseUrl: 'https://www.befolkningibalance.dk',
  menuPageKeyList: [
    'fakta',
    'politik',
    'inspiration',
    'links',
    'svar',
    'blog',
    'om',
  ],
  title: 'Befolkning i balance',
};