---
layout: page
pagekey: inspiration
permalink: /inspiration/
tags: page
title: Inspiration
---

# Cases, kunst, litteratur og traditioner

På siden ["Links"](../links) henviser vi til en række organisationer, som arbejder i dybden med befolknings- og fødsels-problemstillinger.

Her nedenfor er links til kilder, der beskæftiger sig med befolkningsspørgsmål på mere engagerende og tankevækkende måder. Hvis du har et tip om en god historie eller et spændende værk, vil vi meget gerne have det med på listen. Send mail til os på befolkningibalance@gmail.com. 

## Kunstneriske fremstillinger
[En refleksions-film](https://www.outsideonline.com/2398949/mindless-crowd)  med tanker om befolkningen og Jordens landskaber (4 minutter, på engelsk, gratis).

[Filmen "2040"](https://www.dr.dk/drtv/program/2040-_-filmen-om-fremtiden_183651) er en fars forsøg på at fremmane en positiv fremtid for sin datter. Let at følge og godt grundlag for en snak blandt familien eller en gruppe venner (1½ time, engelsk, gratis at se fra Danmark på DR).

[Bogen "10:59"](https://www.goodreads.com/book/show/54344150-10) af N.R. Baker. Letlæst (ungdoms)roman om en ung mand i et rigt land, som bliver en del af en organisation, der vil gøre noget ved overbefolkningen.

[Bogen "Fever"](https://www.goodreads.com/book/show/34273721-fever) af Deon Meyer. Roman om en pandemi, der udrydder en stor del af verdens befolkning. Foregår i Sydafrika og er både spændende og smuk trods det dystre emne.

## Dokumentarfilm
["Endgame 2050"](https://www.youtube.com/watch?v=o8YomEOExkc) undersøger, hvordan det ser ud med Jordens ressourcer de næste 30 år. 
Instruktøren siger: "Mange film om miljøet sætter situationen i et rosenrødt skær for ikke at skræmme folk. Jeg lavede denne film for at vise situationens alvor, så vi kan se den i øjnene." (1½ time, på engelsk, gratis)

[“Our Gorongosa”](https://www.youtube.com/watch?v=BrsgYqn_v1o) viser, hvordan Gorongosa-nationalparken i Mozambique bliver etableret som et skoleeksempel på samarbejde med lokalbefolkningen. Nationalparkens samarbejde med de lokale indbyggere lægger vægt på pigers uddannelse og kvinders rettigheder (1 time, på engelsk og med engelske undertekster, gratis)

["8 billion angels"](https://8billionangels.org/) ser på overbefolkningen og hvad vi kan gøre. Instruktøren siger: "Jeg vil gerne dele et håb på baggrund af almindelige menneskers historier. Det er først når vi er klar til at tale højt og ærligt om et problem, at vi kan begynde at gøre noget ved det." (1 time og 20 minutter, på engelsk, on-demand visning koster 10 USD)

[Growthbusters](https://www.growthbusters.org/watch-growthbusters/) handler om en mand i et lokalsamfund, som vælger at stille spørgsmålstegn ved kommunens evige udstykninger til nyt byggeri. (1 time, på engelsk, download for 10 USD)

## Aktuelle succeshistorier
**Botswana** har i mange år sikret prævention og familieplanlægningsydelser til hele befolkningen, også på landet. Botswana har haft en af verdens hurtigst faldende fødselskurver, fra 7 børn pr kvinde til nu under 3. Læs [artikel fra det britiske nyhedsmedie The Guardian](https://www.theguardian.com/world/2018/oct/10/how-to-grapple-with-soaring-world-population-an-answer-from-down-south).

## Kendte mennesker

Naturkenderen David Attenborough, som har indspillet mange fantastiske naturfilm, har i de seneste år udtalt sig meget klart om behovet for at sænke Jordens befolkningstal. Mange streamingtjenester giver mulighed for at se nogle af hans film. Her er forfilmen til hans foreløbig sidste film (han er 95 år gammel!), ["Et liv på vores planet"](https://www.attenboroughfilm.com/)

Racerkøreren Leilani Münther er veganer, klimakompenserer for benzinen i sin racerbil, og arbejder for at udbrede budskabet om, hvordan befolkningstallet presser Jordens ressourcer. På [hendes hjemmeside](http://www.leilani.green/) kan du se film, artikler og podcast om hende.

Storbrittaniens nuværende premierminister, Boris Johnson, skrev [denne tale](https://www.boris-johnson.com/2007/10/25/global-population-control/) om overbefolkningsproblemerne allerede i 2007, da han var borgmester i London.

Klimaforskeren Kimberley Nicholas er medforfatter på et studie, der viser at antal børn på langt sigt er det mest klima-påvirkende valg, en privatperson kan tage. [Her fortæller hun](https://www.facebook.com/watch/?v=2000661693308775), hvordan hun selv sænker sit klima-aftryk. 

## Mere inspiration

[Fablen om de vilde katte](https://realisticpragmatism.wordpress.com/2018/06/03/the-stray-cat-dog-analogy-to-overpopulation/) illustrerer, hvordan man som godhjertet donor, der gør kortsigtet gavn, kan gøre langsigtet skade, hvis ikke man tænker på helheden (blog på engelsk).
