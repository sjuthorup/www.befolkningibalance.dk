---
layout: page
pagekey: om
permalink: /om/
tags: page
title: Om
---

## Vær med i en konstruktiv debat

Formålene med oplysningsinitiativet ‘Befolkning i balance’ er:

- At af-tabuisere befolkningstal som emne i den danske samfundsdebat.

- At pege på samfundsområder, hvor en politik for befolkningens størrelse er relevant.

- At bidrage med saglig information om befolkningsspørgsmål.

- At inspirere NGO’er, politiske partier og andre tænkende medborgere til at have en holdning til befolkningstal og fødselstal.

- At medvirke til, at Danmark får en seriøs befolkningspolitik.

Vi er et uformelt netværk af borgere, som er bekymrede for civilisationens rovdrift på Jordens klima og natur. Vi deltager i den danske og europæiske miljø- og klima-bevægelse, og vi mener at befolkningstallet er en vigtig faktor i debatten, som ofte overses eller ignoreres.

Vi har foreløbig valgt ikke at etablere os som en forening. Fremfor at bruge tid på administration vil vi hellere koncentrere os maksimalt om debatskabende aktiviteter.
Hvis du vil være med i vores opmærksomhedsskabende arbejde, så kontakt os. Sig også til, hvis du har spørgsmål, kommentarer eller input til hjemmesiden. Vi glæder os til at tale med dig.

Send en mail til befolkningibalance@gmail.com – Vær tålmodig, vi tjekker den ikke altid hver dag. 
Vi har ingen kontakttelefon, fordi dette er et fritidsprojekt. Lad os gerne skrive sammen og aftale at snakke i telefon.

Aktive i netværket:

* Sju G. Thorup (talsperson), cand.scient, MBA, [ledelseskonsulent](https://sju.xpqf.com/da/). Sju har været aktiv i Klimabevægelsen, hvor hun startede den ugentlige aktion [Klimapåmindelsen](https://www.facebook.com/events/444908466742115/), og i det nye parti De Grønne, hvor hun var drivkraft i udviklingen af [De Grønnes 2040-vision](https://degronne.dk/2040-vision/). Sju laver også en web-tegneserie om vores positive grønne fremtid: [Fremtidsglimt](https://www.futureglimpses.earth/) (dansk tekst under billederne). 

* Thomas Meinert Larsen, PhD, projektleder. Thomas var medstifter af [Klimabevægelsen](https://www.klimabevaegelsen.dk/) og er stadig medlem af bestyrelsen. Han har været ernæringsforsker og arbejder nu som kampagneleder for [Ansvarlig Fremtid](https://www.ansvarligfremtid.dk/ansvarligpension/), som organiserer initiativer i de danske pensionskasser for at investere klimavenligt.

* Lars Thorup, datalog, selvstændig [softwareudvikler](https://stackoverflow.com/cv/larsthorup). Lars står for den tekniske implementering af hjemmesiden. Lars og Sju er gift.

![portrætfotos af Sju, Thomas, Lars](./sju-thomas-lars-fotos.PNG)