# DNS configuration

## DNS

for befolkningibalance.dk

- A `@` - `192.0.2.1` - enable cloudflare proxying
- CNAME `www` - `www.befolkningibalance.dk.s3-website.eu-central-1.amazonaws.com`
- forward `befolkningibalance.dk/*` to `www.befolkningibalance.dk/$1`
- Always use HTTPS: `http://*.befolkningibalance.dk/*`
