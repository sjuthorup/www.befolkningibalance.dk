# www.befolkningibalance.dk website

```bash
npm install
npm start
```

[Markdown reference](https://markdown-it.github.io/)

[Deployment setup](./doc/aws.md)

[DNS setup](./doc/dns.md)
